import config
import telebot
import schedule
import time
import os
import psycopg2
from collections import defaultdict
from random import randint


def parser_job():
    print('Working!')
    os.system('python3 parser/anime_parser.py')
schedule.every().day.at('00:00').do(parser_job)

top_anime = dict()
top_shonen_anime = dict()
watched_anime_series = defaultdict(dict)

try:
    connection = psycopg2.connect(user=config.postgres['login'],
                                  password=config.postgres['password'],
                                  host=config.postgres['host'],
                                  port=config.postgres['port'],
                                  database=config.postgres['db_name'])

    cursor = connection.cursor()
    postgreSQL_select_Query = "select * from anime_series"
    cursor.execute(postgreSQL_select_Query)
    top_anime_records = cursor.fetchall()
except (Exception, psycopg2.Error) as error:
    print("Error while fetching data from PostgreSQL", error)
finally:
    if connection:
        cursor.close()
        connection.close()
        print("PostgreSQL connection is closed")

total_num = 0
shonen_num = 0
for anime_row in top_anime_records:
    top_anime[total_num] = dict()
    top_anime[total_num]['cover'] = anime_row[1]
    top_anime[total_num]['title'] = anime_row[2]
    top_anime[total_num]['rating'] = anime_row[3]
    top_anime[total_num]['genres'] = anime_row[4]
    total_num += 1
for anime in top_anime.values():
    if 'Сёнэн' in anime['genres']:
        top_shonen_anime[shonen_num] = dict()
        top_shonen_anime[shonen_num] = anime
        shonen_num += 1

# telebot.apihelper.proxy = {'https': config.proxy}
bot = telebot.TeleBot(config.token)

for top_anime_num in top_anime:
    anime_type = 'top_anime'
    if watched_anime_series.get(anime_type) is None: watched_anime_series[anime_type] = dict()
    watched_anime_series[anime_type][top_anime[top_anime_num]['title']] = False
for shonen_anime_num in top_shonen_anime:
    anime_type = 'shonen'
    if watched_anime_series.get(anime_type) is None: watched_anime_series[anime_type] = dict()
    watched_anime_series[anime_type][top_shonen_anime[shonen_anime_num]['title']] = False


# compare random anime with watched anime list
def compare_anime(watched_list, random_anime, anime_type):
    if anime_type == 'shonen':
        anime_title = top_shonen_anime[random_anime]['title']
        if watched_list[anime_title] is True:
            random_anime = randint(0, len(top_shonen_anime) - 1)
            if False not in watched_list.values():
                return False
            else:
                return compare_anime(watched_list, random_anime, anime_type)
        else:
            return random_anime
    elif anime_type == 'top_anime':
        anime_title = top_anime[random_anime]['title']
        if watched_list[anime_title] is True:
            random_anime = randint(0, len(top_anime) - 1)
            if False not in watched_list.values():
                return False
            else:
                return compare_anime(watched_list, random_anime, anime_type)
        else:
            return random_anime


# send anime description by anime type
def send_anime_description(anime_type, anime_list, chat_id):
    keyboard = show_message_keyboard(anime_type)
    random_anime = randint(0, len(anime_list) - 1)
    random_anime = compare_anime(watched_anime_series[anime_type], random_anime, anime_type)
    if random_anime is False:
        bot.send_message(chat_id, 'Congratulation, you watched all titles already!')
    else:
        watched_anime_count = 0
        for anime in watched_anime_series[anime_type]:
            if watched_anime_series[anime_type][anime] is False: continue
            watched_anime_count += 1

        bot.send_photo(chat_id, anime_list[random_anime]['cover'],
                       caption=f"Title: {anime_list[random_anime]['title']}\n"
                               f"Rating: {anime_list[random_anime]['rating']}\n"
                               f"Genres: {', '.join(anime_list[random_anime]['genres'])}\n" 
                               f"Watched {anime_type} titles: {watched_anime_count}", reply_markup=keyboard)


# show message keyboard under each anime series to make 'watched' button
def show_message_keyboard(anime_type):
    keyboard = telebot.types.InlineKeyboardMarkup()
    callback_button = telebot.types.InlineKeyboardButton(text="Watched", callback_data=anime_type)
    keyboard.add(callback_button)
    return keyboard


@bot.message_handler(commands=['start'])
def show_keyboard(message):
    keyboard = telebot.types.ReplyKeyboardMarkup(True)
    shonen_button = telebot.types.InlineKeyboardButton(text="Recommend shonen series")
    top_anime_button = telebot.types.InlineKeyboardButton(text="Recommend top anime series")
    keyboard.row(shonen_button, top_anime_button)
    bot.send_message(message.chat.id, 'Choose what to recommend', reply_markup=keyboard)


@bot.callback_query_handler(func=lambda call: True)
def query_handler(call):
    bot.answer_callback_query(callback_query_id=call.id, text='Added to watched')
    rows = call.message.caption.split('\n')
    message = dict()
    for row in rows:
        name, caption = row.split(':', maxsplit=1)
        message[name] = caption
    anime_title = message['Title'].strip()
    if call.data:
        watched_anime_series[call.data][anime_title] = True


@bot.message_handler(content_types=['text'])
def random_anime_message(message):
    if message.text == 'Recommend shonen series':
        anime_type = 'shonen'
        send_anime_description(anime_type, top_shonen_anime, message.chat.id)
    elif message.text == 'Recommend top anime series':
        anime_type = 'top_anime'
        send_anime_description(anime_type, top_anime, message.chat.id)


if __name__ == '__main__':
    schedule.run_pending()
    time.sleep(1)
    bot.infinity_polling()
