from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import sessionmaker
from sqlalchemy.dialects import postgresql
import requests
import config
import time
from bs4 import BeautifulSoup

CURRENT_TIME = time.time()
URL = 'https://yummyanime.club/top'
HEADERS = {
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36',
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9'
}
HOST = 'https://yummyanime.club'

engine = create_engine(f"postgresql+psycopg2://{config.postgres['login']}:{config.postgres['password']}@/{config.postgres['db_name']}")
Base = declarative_base()
Session = sessionmaker(bind=engine)
session = Session()


class AnimeSeries(Base):
    __tablename__ = 'anime_series'

    id = Column(Integer, primary_key=True)
    cover = Column(String)
    title = Column(String)
    rating = Column(String)
    genres = Column(postgresql.ARRAY(String))


Base.metadata.create_all(engine)


def get_html(url, params=None):
    r = requests.get(url, headers=HEADERS, params=params)
    return r


def get_anime_series_content(html):
    soup = BeautifulSoup(html, 'html.parser')
    item = soup.find('div', class_='content-page anime-page')

    genre_list = item.find('li', class_='categories-list').find('ul').text.splitlines()
    while '' in genre_list:
        genre_list.remove('')
    return genre_list


def get_content(html):
    soup = BeautifulSoup(html, 'html.parser')
    items = soup.find_all('div', class_='anime-column')
    times = 0
    for item in items:
        anime_url = item.find('a', class_='image-block').attrs['href']
        anime_html = get_html(HOST + anime_url)
        if anime_html.status_code == 200:
            genres = get_anime_series_content(anime_html.text)
            times += 1
            if times % 3 == 0:
                time.sleep(3)
        else:
            raise Exception(f'Error {times}')
        if session.query(AnimeSeries).filter_by(title=item.find('a', class_='anime-title').get_text(strip=True)).scalar() is not None: continue
        session.add(AnimeSeries(cover=HOST + item.find('a', class_='image-block').img['src'],
                                title=item.find('a', class_='anime-title').get_text(strip=True),
                                rating=item.find('span', class_='main-rating').get_text(strip=True),
                                genres=genres))

    session.commit()


def parse():
    html = get_html(URL)
    if html.status_code == 200:
        get_content(html.text)
    else:
        raise Exception('Error')

parse()
print('PARSED!')
print(time.time() - CURRENT_TIME)