beautifulsoup4==4.9.0
PySocks==1.7.1
pyTelegramBotAPI==3.7.1
requests==2.23.0
SQLAlchemy==1.3.17
psycopg2-binary==2.8.5